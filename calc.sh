#!/bin/sh

cat <<EOF
shcalc v1.1
EOF

echo -n '> '
while read input; do
	env -i sh -c "echo \$(($input))"
	echo -n '> '
done
